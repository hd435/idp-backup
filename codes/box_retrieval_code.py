import math
import numpy as np
import matplotlib.pyplot as plt


#coordinates of the walls, measured using gps
#increment can be changed, however make sure all the arrays are of same length
wall1x = np.arange(-1.16,1.18,0.01) #all the x coordinates of wall1 - y is constant
wall2y = np.arange(-1.17,1.17,0.01) #all the y coordinates of wall2 - x is constant
wall3x = np.arange(-1.16,1.18,0.01) #all the x coordinates of wall3 - y is constant
wall4y = np.arange(-1.17,1.17,0.01) #all the y coordinates of wall4 - x is constant

wall1coordinates = []
wall2coordinates = []
wall3coordinates = []
wall4coordinates = []

#note: (x is Z in webots and y is X in webots)
for i in range (len(wall1x)):
    wall1coordinates.append([wall1x[i],-1.17])
    wall2coordinates.append([1.18,wall2y[i]])
    wall3coordinates.append([wall3x[i],1.17])
    wall4coordinates.append([-1.16,wall4y[i]])
#print(wall1coordinates)

wallcoordinates_all= []
for i in range (len(wall1coordinates)):
    wallcoordinates_all.append(wall1coordinates[i])
    wallcoordinates_all.append(wall2coordinates[i])
    wallcoordinates_all.append(wall3coordinates[i])
    wallcoordinates_all.append(wall4coordinates[i])

print(len(wallcoordinates_all))

#coordinate of the robot - example
robot_coord = [-0.4,0.00985]


distance_wall_from_robot_vec = []
#magnitude of all the distances from location of robot to the 4 walls
distance_wall_from_robot_mag = []


for i in range (len(wallcoordinates_all)):
    distance_wall_from_robot_vec.append([wallcoordinates_all[i][0]-robot_coord[0],wallcoordinates_all[i][1]-robot_coord[1]])
for i in range(len(distance_wall_from_robot_vec)):
    distance_wall_from_robot_mag.append(np.linalg.norm(distance_wall_from_robot_vec[i]))
    

#print(distance_wall_from_robot_mag)

xscale = np.arange(0,936,1)
#print(len(xscale)
plt.plot(xscale,distance_wall_from_robot_mag)
plt.show()