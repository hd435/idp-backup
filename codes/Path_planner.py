import matplotlib.pyplot as plt
import numpy as np
import math
### FUNCTION STILL NEEDS WORK
### Path will only change once, and there are some combination of obstacles that don't work
### Also doesn't work if starting point has an x or y coordinate which is equal to the coordinates of destination 
def isInside(point, edge_1, edge_2):
    if edge_1[0] > edge_2[0]:
        if point[0] > edge_1[0] or point[0] < edge_2[0]:
            return False
        else:
            return True
    
    elif edge_1[0] < edge_2[0]:
        if point[0] < edge_1[0] or point[0] > edge_2[0]:
            return False
        else:
            return True
    
    elif edge_1[1] > edge_2[1]:
        if point[1] > edge_1[1] or point[1] < edge_2[1]:
            return False
        else:
            return True
    
    elif edge_1[1] < edge_2[1]:
        if point[1] < edge_1[1] or point[1] > edge_2[1]:
            return False
        else:
            return True
    
def get_intersections(x0, y0, r0, x1, y1, r1):
    # circle 1: (x0, y0), radius r0
    # circle 2: (x1, y1), radius r1
    d=math.sqrt((x1-x0)**2 + (y1-y0)**2)
    a=(r0**2-r1**2+d**2)/(2*d)
    h=math.sqrt(r0**2-a**2)
    x2=x0+a*(x1-x0)/d   
    y2=y0+a*(y1-y0)/d   
    x3=x2+h*(y1-y0)/d     
    y3=y2-h*(x1-x0)/d 

    x4=x2-h*(y1-y0)/d
    y4=y2+h*(x1-x0)/d
        
    return ([x3, y3], [x4, y4])

##this finds the minimum distance and angles between the path the robot takes and obstacles
def find_min_distances_and_angles(destination, obstacle_coords, starting_position):
    mindistances = []
    path_obstacle_angles = []
    for i in range (len(obstacle_coords)):
        b = isInside(obstacle_coords[i], starting_position, destination)
        if b is True:
            p1 = np.array(starting_position)
            p2 = np.array(destination)
            p3 = np.array(obstacle_coords[i])
            mindistances.append((np.cross(p2-p1,p3-p1)/np.linalg.norm(p2-p1)))
            length = math.sqrt((p3[0] - p1[0])**2 + (p3[1] - p1[1])**2)
            path_obstacle_angles.append(np.arcsin((abs(mindistances[-1])/length)) * 180/math.pi)
        else:
            pass
    distance_angle = [{n:m} for n,m in zip(mindistances,path_obstacle_angles)]
    return mindistances, path_obstacle_angles
    
## radius is how close path is allowed to get to obstacle, a is control variable which determines
## how far from obstacle the path will move. both can be changed and will produce different effects, need to tweak a
def routeplanner(destination, obstacle_coords, starting_position, radius = 1, a = 1):
    target_x = []
    target_y = []
    n_clear = 0
    if len(mindistances) == 0:
        target_x.append(destination[0])
        target_y.append(destination[1])
        return target_x, target_y
    for i in range(len(path_obstacle_angles)):

        p1 = np.array(starting_position)
        p2 = np.array(destination)
        p3 = np.array(obstacle_coords[i])
        length = math.sqrt((p3[0] - p1[0])**2 + (p3[1] - p1[1])**2)
        max_theta = np.arcsin(radius/ length)
        length = math.sqrt((p3[0] - p1[0])**2 + (p3[1] - p1[1])**2)
        ksi = length * math.cos(max_theta)
        length_2 = a + radius
        length_3 = math.sqrt(a**2 + ksi**2)
        if mindistances[i] > radius:
            n_clear = n_clear + 1
        else:
            print(p3)
            new_start_1, new_start_2 = get_intersections(p1[0],p1[1],length_3,p3[0],p3[1],length_2)
            print(new_start_1, new_start_2)
            mindistances_1, path_obstacle_angles_1 = find_min_distances_and_angles(destination, obstacle_coords, new_start_1)
            mindistances_2, path_obstacle_angles_2 = find_min_distances_and_angles(destination, obstacle_coords, new_start_2)
            print(mindistances_1)
            print(len(mindistances_1))
            if len(mindistances_1) == 0:
                target_x.append(new_start_1[0])
                target_y.append(new_start_1[1])
                print(target_x,target_y)
            elif len(mindistances_2) == 0:
                target_x.append(new_start_2[0])
                target_y.append(new_start_2[1])
            else:
                for i in range(len(mindistances_1)):
                    if mindistances_1[i] > radius:
                        target_x.append(new_start_1[0])
                        target_y.append(new_start_1[1])
                    elif mindistances_2[i] > radius:
                        target_x.append(new_start_2[0])
                        target_y.append(new_start_2[0])
                    else:
                        print('oh no')
    
    if n_clear == len(mindistances):
        target_x.append(destination[0])
        target_y.append(destination[1])
        return target_x, target_y

    else:
        return target_x, target_y


#Vary starting position
starting_position = [0,0]
destination = [4,4]
obstacle_coords = ([2,2], [2.5,2],[1.5,1.8], [3,4])
mindistances, path_obstacle_angles = find_min_distances_and_angles(destination, obstacle_coords,starting_position)
target_x, target_y = routeplanner(destination,obstacle_coords,starting_position)

#this plots the route on a graph
print(target_x, target_y)
mid_target = [target_x[-1], target_y[-1]]
x, y = zip(*obstacle_coords)
plt.plot(x ,y, 'ro')
plt.plot(starting_position[0],starting_position[1], '^')
plt.plot(destination[0],destination[1], 's')
plt.plot([starting_position[0],mid_target[0]], [starting_position[1],mid_target[1]])
plt.plot([mid_target[0], destination[0]], [mid_target[1], destination[1]])
plt.plot(mid_target[0], mid_target[1], '*')
plt.show()

##This function needs work, priority is to allow for path to change more than once,
##Particular placements will bug out the function, eg if there is an obstacle on where the coordinate the path wants to change to, the function just bugs out

