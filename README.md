# IDP BACKUP

Second year integrated design project. Students work in teams of six to design, build and test a mobile robot vehicle as an integrated design project (IDP). Due to covid reason testing of the vehicle is done virtually in webot environment.

I was in charge of algorithm and robot building in our team of 6. In final competition, our team achieved 1st runner-up among all teams in our cohort.
