from controller import Robot, Motor, Compass, GPS, Camera, Emitter, Receiver, Display
import time
import math
import numpy as np
import matplotlib.pyplot as plt


robot = Robot()

timestep = 16

#emitter receiver
emitter = robot.getDevice('emitter')
receiver = robot.getDevice('receiver')
#Drive motors
Lmotor = robot.getDevice('LMOTOR')
Rmotor = robot.getDevice('RMOTOR')
#Platform raising and lowering motor
Pmotor = robot.getDevice('PLATFORMMOTOR')
#linear actuators for pincers
Larm = robot.getDevice('LGRABBERACTUATOR')
Rarm = robot.getDevice('RGRABBERACTUATOR')

motor_list = [Larm, Rarm, Pmotor, Lmotor, Rmotor]
for motor in motor_list:
    motor.setPosition(float('inf'))
    motor.setVelocity(0)

#Camera
camera = robot.getDevice('camera')
#Sensors
Rds = robot.getDevice('RDISTANCESENSOR')  #Long range
Lds = robot.getDevice('LDISTANCESENSOR')  #Short range
Comp = robot.getDevice('COMPASS')
Gps = robot.getDevice('GPS')
displ = robot.getDevice('display')

#Emitter, receiver
emitter = robot.getDevice('emitter')
receiver = robot.getDevice('receiver')
sensor_list = [Rds, Lds, Comp, Gps, camera]
for device in sensor_list:
    device.enable(timestep)


#initialising lists to be used
displacementx = []
displacementy = []
angles_list = []
real_longrange = []
real_shortrange = []
predicted_distance = []
obstacle_coords = []

xcoordinatesscanned = []
ycoordinatesscanned = []
Xpossibleboxcoord=[]
Ypossibleboxcoord=[]

def record_lists():
    #updates ALL lists, put in EVERY while loop in any piece of code to ensure it's always updating!!!
    displacementx.append((gpsrecord())[0])
    displacementy.append((gpsrecord())[1])
    angles_list.append(comprecord())
    real_longrange.append(Rlong_dist())
    real_shortrange.append(Lshort_dist())
    predicted_distance.append(predict_distance())

def gpsrecord():
    #gives the current (x,y) coordinates of the robot when called
    #y IS THE NORTH-FACING DIRECTION WHEN THE RED SQUARE IS IN THE EAST AND GREEN IN THE WEST
    record = Gps.getValues()
    return [record[2], record[0]]

def comprecord():
    #gives the current angle in the plane of motion of the robot when called
    record = Comp.getValues()
    rad = math.atan2(record[0], record[2])
    bearing = (rad - np.pi/2) / np.pi * 180.0
    if bearing < -180:
        bearing += 360
    return bearing

def Lshort_dist():
    #distance in meters from short range distance sensor
    distance_short = 0.1594*(Lds.getValue()**-0.8533)-0.02916
    return distance_short

def Rlong_dist():
    #distance in meters from long range distance sensor, 0.94 default 0.9313
    distance_long = 0.7611*(Rds.getValue()**-0.94)-0.1252
    return distance_long

def coord_to_angle(coord):
    x, y = coord[0], coord[1]
    if x > 0 and y > 0:
        angle = math.degrees(math.atan(x/y))
    elif x < 0 and y < 0:
        angle = math.degrees(math.atan(x/y)) - 180
    elif x < 0 and y > 0:
        angle = angle = math.degrees(math.atan(x/y))
    elif x > 0 and y < 0:
        angle = math.degrees(math.atan(x/y)) + 180
    return angle

def sort_by_angle(coords):
    angle = []
    for coord in coords:
        angle.append(coord_to_angle(coord))
    result = [x for _,x in sorted(zip(angle,coords))]
    return result

def predict_distance(wc = 1.16):
    #gives the predicted distance to the wall in the direction the robot is pointing in
    cur_coord = gpsrecord()
    cur_angle = comprecord()

    walls_clockwise_from_top = [[(-wc, wc), (wc, wc)], [(wc, wc), (wc, -wc)], [(wc, -wc), (-wc, -wc)], [(-wc, -wc), (-wc, wc)]]

    #first determine which wall is being pointed at
    for i in range(4):
        wall_angles = []
        wall = walls_clockwise_from_top[i]

        for wall_point in wall:
            vector = np.array(wall_point) - np.array(cur_coord)
            vector_angle = math.atan2(vector[0], vector[1]) / np.pi * 180.0

            wall_angles.append(vector_angle)

        if cur_angle > wall_angles[0] and cur_angle < wall_angles[1]:
            wall_in_front = i + 1
            break

        elif i == 2:
            if cur_angle > wall_angles[0] and cur_angle < 180 or cur_angle < wall_angles[1] and cur_angle > -180:
                wall_in_front = 3
                break
    
    cur_angle = cur_angle*np.pi/180

    if wall_in_front == 1:
        y = wc - cur_coord[1]
        d = y/np.cos(cur_angle)
    if wall_in_front == 2:
        x = wc - cur_coord[0]
        d = x/np.sin(cur_angle)
    if wall_in_front == 3:
        y = cur_coord[1] + wc
        d = y/np.cos(cur_angle)
    if wall_in_front == 4:
        x = cur_coord[0] + wc
        d = x/np.sin(cur_angle)
    return abs(d)

def scan360 (clockwise = True, speed = 2, spin_time_constant = 700, block_detection_threshold = 0.05, Lrange_cutoff = 1.39, Srange_cutoff = 0.39, sensors_to_gps = 0.06):
    #makes the robot spin 360 and record any potential blocks as lists of distances and angles for each block
    if clockwise == False:
        speed = -speed

    spin_time = int(spin_time_constant/speed)
    counter = 0
    
    #lists for angle, long range, short range and predicted distance for spotting blocks later

    block = False
    block_data = []
    all_blocks_data = []

    while counter <= spin_time:
        record_lists()
        robot.step(timestep)
        counter += 1

        Lmotor.setVelocity(speed)
        Rmotor.setVelocity(-speed)

        Long = Rlong_dist() + sensors_to_gps
        Short = Lshort_dist() + sensors_to_gps
        Predict = predict_distance()
        Angle = comprecord()

        if Long > Lrange_cutoff and Short > Srange_cutoff:
            #if sensing above cutoff range, no block and skip this recording, 
            #if was prev. looking at block, append block data to list of all blocks and clear dataset
            block = False
            if len(block_data) != 0:
                all_blocks_data.append(block_data)
                block_data = []
        
        elif block == False and Predict - Long > block_detection_threshold:
            #if first block reading found, true to looking at block, append data
            block = True
            block_data.append([Long, Angle])
        
        elif block == True:
            #if already looking at block
            if Predict - Long < block_detection_threshold:
                #no longer sees block, append dataset to list of all blocks, clear dataset for next block
                block = False
                all_blocks_data.append(block_data)
                block_data = []
            else:
                #block true, in range and detection above threshold, keep appending data to block data
                block_data.append([Long, Angle])
    
    Rmotor.setVelocity(0)
    Lmotor.setVelocity(0)

    if len(block_data) != 0:
        all_blocks_data.append(block_data)
    #print("Before: ", len(all_blocks_data))
    return all_blocks_data

def red_remove_blocked(all_block_data):
    list_of_removal = []
    for data in all_block_data:
        if data[0][1] < -70.5 and data[0][1] > -109.5:   #remove blocked box
            if data[0][0] > 0.5 and data[0][0] < 0.9: 
                list_of_removal.append(data)
    for removal in list_of_removal:
        all_block_data.remove(removal)    
    return all_block_data

def green_remove_blocked(all_block_data):
    list_of_removal = []
    for data in all_block_data:
        if data[0][1] > -45 or data[0][1] < -135:   #remove redundant data
            list_of_removal.append(data)
    for removal in list_of_removal:
        all_block_data.remove(removal)
    return all_block_data
        
        
def turnto(angle, speed = 5, error = 0.1, k = 0.05, negation_avoidance_threshold = 160):
    #turns to specified angle with specified speed, issue with turning more than 180 or -180 degrees so use range (-180 ==> 180) ONLY
    while True:
        robot.step(timestep)
        cur_angle = comprecord()
        if angle > negation_avoidance_threshold:
            if cur_angle < -negation_avoidance_threshold:
                cur_angle += 360
            else: pass
        elif angle < -negation_avoidance_threshold:
            if cur_angle > negation_avoidance_threshold:
                cur_angle -= 360
            else: pass
        else: pass

        delta = angle - cur_angle

        if abs(delta)>error:
            vel = k*delta*speed
            if vel >= speed:
                vel = speed
            if vel <= -speed:
                vel = -speed
            Lmotor.setVelocity(vel)
            Rmotor.setVelocity(-vel)

        else:
            Lmotor.setVelocity(0)
            Rmotor.setVelocity(0)
            break
    return None

def turn_to_coords(coords):
    #turn to a coordinate but dont move yet
    vector = np.array(coords) - np.array(gpsrecord())
    vector_angle = math.atan2(vector[0], vector[1]) / np.pi * 180.0
    if vector_angle < -180:
        vector_angle += 360
    turnto(vector_angle)
    return None
    
def moveto(coords, speed = 10, error = 0.008, k = 10):
    #moves in a straight line to input destination coordinate with specified speed
    turn_to_coords(coords)
    initial_heading = np.array(coords) - np.array(gpsrecord())
    heading_unit_vector = initial_heading/np.linalg.norm(initial_heading)

    while True:
        #print(gpsrecord(), vector_angle)
        robot.step(timestep)
        vector = np.array(coords) - np.array(gpsrecord())
        distance = np.dot(vector, heading_unit_vector)/np.linalg.norm(heading_unit_vector)

        if distance>error:
            vel = k*distance*speed
            if vel >= speed:
                vel = speed
            if vel <= -speed:
                vel = -speed
            Lmotor.setVelocity(vel)
            Rmotor.setVelocity(vel)
            avoid_collision()

        else:
            Lmotor.setVelocity(0)
            Rmotor.setVelocity(0)
            break

    return None

def approach_and_catch(coords, colour, tol = 0.5, counters = 1):
    #this function should only be implemented after robot arrives the ring area where both sensors can detect the block wanted
    #it will guide the robot to catch and store the wanted box and stop there
    turn_to_coords(coords)
    l_dist = Lshort_dist()
    r_dist = Rlong_dist()
    cur_coord = gpsrecord()
    expected_dist = ((coords[0] - cur_coord[0])**2 + (coords[1] - cur_coord[1])**2 )**0.5
    counter = counters
    robot.step(timestep)
    
    if abs(expected_dist - r_dist) < tol and abs(expected_dist - l_dist) < tol and detect_colour() == colour:  # 
        print("Expected box found. ", colour)
        while ready_to_catch() == False:
            robot.step(timestep)
            record_lists()
            Lmotor.setVelocity(5)
            Rmotor.setVelocity(5)
        Lmotor.setVelocity(0)
        Rmotor.setVelocity(0)
        print("Robot ready to catch")
        catch_store()
        return True
        
        
    else:
        # do a small angle scan in case coords of the expected block have big error
        # if not, give up this block, navigate to next target
        # only scan small angle range since the error is unlikely to be big
        counter -= 1
        if counters < 0:
            robot.step(timestep)
            print('Expected block not found')
            return False
        else:
            robot.step(timestep)
            coords = np.array(coords)
            cur_coord = np.array(cur_coord)
            unit_vector = (coords - cur_coord)/np.linalg.norm(coords - cur_coord)
            normal_unit_vector = np.array([-unit_vector[1], unit_vector[0]])
            normal_vector = normal_unit_vector/40 #divide by 25 is 0.04m, slightly smaller than lenght of box
            new_coords_l = coords + normal_vector
            new_coords_r = coords - normal_vector
            if approach_and_catch(new_coords_l, colour = colour, tol = 0.2, counters = counter) == False:
                approach_and_catch(new_coords_r, colour = colour, tol = 0.2, counters = counter)           
     
    
def catch_store(stable = 60, arm = 60, plat = 80):   #constants inversely proportional to timestep
    #t_arm, t_platform define counters for movement of arm, platform
    counter = 0
    Larm.setVelocity(0)
    Rarm.setVelocity(0)
   
    while counter < 280:
        robot.step(timestep)
        counter += 1  
        if counter < stable:    #stabalise block
            Larm.setVelocity(-0.05)
            Rarm.setVelocity(0.05)
            """
            if counter < stable/2.5:
                Larm.setVelocity(-0.03)
                Rarm.setVelocity(0)
            elif counter < stable/2.0:
                Larm.setVelocity(0)
                Rarm.setVelocity(0)
            elif counter < stable/1.5:
                Larm.setVelocity(0)
                Rarm.setVelocity(0.03)
            else:
                Larm.setVelocity(0.0)
                Rarm.setVelocity(-0.0)
            """
        elif counter < arm + stable: #catch
            Larm.setVelocity(-0.05)
            Rarm.setVelocity(0.05)   
        elif counter < arm + plat  + stable - 15: #rotate for storing
            # 10 above is for throwing box so that it won't land on car body
            Pmotor.setVelocity(-2.2)
            Larm.setVelocity(-0.015)
            Rarm.setVelocity(0.015)
        elif counter < arm + plat + stable: #unload
            Pmotor.setVelocity(-2.2) 
            Larm.setVelocity(0.15)
            Rarm.setVelocity(-0.15)     
        else:
            Pmotor.setVelocity(0)
            Pmotor.setVelocity(2.2)
            Larm.setVelocity(0)
            Rarm.setVelocity(0)
            not_complete = False
    Pmotor.setVelocity(0)
    Larm.setVelocity(0)
    Rarm.setVelocity(0)    
    return True

def ready_to_catch(l_thres = [0.065,0.085], r_thres = 0.180):
    #return True if catch can succeed at the current state
    #will be a part of a function for path adjustment to reach a block
    dist_l = Lshort_dist()
    dist_r = Rlong_dist()
    if dist_r < r_thres and dist_l < l_thres[1] and dist_l > l_thres[0]:
        return True
    else:
        return False

def givecoordofblock(distance_readings, angle_readings):
    #Using list of readings from distance sensor and compass, will give predicted GPS position of block in (x,y)
    current_position_x = gpsrecord()[0]
    current_position_y = gpsrecord()[1]
    #current_position_x = 0
    #current_position_y = 0, used for testing
    x_distances = []
    y_distances = []
    for i in range(len(distance_readings)):
        x_distances.append(distance_readings[i]*(math.sin(math.radians(angle_readings[i]))))
        y_distances.append(distance_readings[i]*(math.cos(math.radians(angle_readings[i]))))
    average_x = sum(x_distances)/len(x_distances)
    average_y = sum(y_distances)/len(y_distances)
    block_position_x = current_position_x + average_x
    block_position_y = current_position_y + average_y

    return [block_position_x, block_position_y]

def coord_detected(d,anglerelativetonorth):
    #this function uses the distance d calculated by a sensor and the angle the robot its at
    #to find the xy coordinate of a point. Note: north is pointing in the +ve X direction in Webots (but its actually y)

    if (anglerelativetonorth<-90 and anglerelativetonorth>=-180):
        x_coord_detected = gpsrecord()[0] - (d*math.sin(math.radians(180-abs(anglerelativetonorth))))
        y_coord_detected = gpsrecord()[1] - (d*math.cos(math.radians(180-abs(anglerelativetonorth))))
        return x_coord_detected, y_coord_detected
    if (anglerelativetonorth<0 and anglerelativetonorth>=-90):
        x_coord_detected = gpsrecord()[0] - (d*math.sin(math.radians(abs(anglerelativetonorth))))
        y_coord_detected = gpsrecord()[1] + (d*math.cos(math.radians(abs(anglerelativetonorth))))
        return x_coord_detected, y_coord_detected
    if (anglerelativetonorth<90 and anglerelativetonorth>=0):
        x_coord_detected = gpsrecord()[0] + (d*math.sin(math.radians(abs(anglerelativetonorth))))
        y_coord_detected = gpsrecord()[1] + (d*math.cos(math.radians(abs(anglerelativetonorth))))
        return x_coord_detected, y_coord_detected
    if (anglerelativetonorth<=180 and anglerelativetonorth>=90):
        x_coord_detected = gpsrecord()[0] + (d*math.sin(math.radians(180-abs(anglerelativetonorth))))
        y_coord_detected = gpsrecord()[1] - (d*math.cos(math.radians(180-abs(anglerelativetonorth))))
        return x_coord_detected, y_coord_detected

def detect_colour():
    image = camera.getImageArray()
    red   = image[32][32][0]
    green = image[32][32][1]

    if (red > 90 and red < 115 and green > 20 and green < 30):
        return "RED"
    if (red > 10 and red < 20 and green < 170 and green > 150):
        return "GREEN"
    else:
        return "NO COLOUR DETECTED"

def ring_coord(target, radius = 0.29):
    #gives coordinate within ring area around a target
    cur_coord = gpsrecord()
    cur_coord = np.array(cur_coord)
    target = np.array(target)
    distance = np.linalg.norm(target - cur_coord)
    unit_vector = (target - cur_coord)/distance
    new_target = (distance - radius) * unit_vector + cur_coord
    return new_target

def GUInterface(arena_width = 1.17):

    #HEXcode of RED 0XFF0000
    #HEXcode of GREEN 0X008000
    #HEXcode of WHITE 0xFFFFFF

    scalex = (displ.getWidth()/2.34)
    scaley = (displ.getHeight()/2.34)

    #maps ALL the points predicted by scan360 (white)
    for i in range (len(all_blocks_data)):
        for j in range(len(all_blocks_data[i])):
            xcoord = (coord_detected(all_blocks_data[i][j][0], all_blocks_data[i][j][1]))[0]
            ycoord = (coord_detected(all_blocks_data[i][j][0], all_blocks_data[i][j][1]))[1]
            displ.setColor(0xFFFFFF)
            pixelx = round((xcoord-(-1.17))*scalex)
            pixely = round((-ycoord+1.17)*scaley)
            displ.drawPixel(pixelx,pixely)

    #maps the average coordinate predicted for each box (yellow)
    for i in range (len(all_blocks_data)):
        distlist = []
        anglelist = []
        for j in range(len(all_blocks_data[i])):
            distlist.append(all_blocks_data[i][j][0])
            anglelist.append(all_blocks_data[i][j][1])
            displ.setColor(0xFFFF00)

        pixelx = round(((givecoordofblock(distlist, anglelist)[0])-(-arena_width))*scalex)
        pixely = round((-(givecoordofblock(distlist, anglelist)[1])+arena_width)*scaley)
        displ.drawPixel(pixelx,pixely)


    #actual block coordinates
    redblockcoordinates = [[-0.273,1.05],[0.606,0.977],[0.571,0.687],[0.42,-0.639]]
    greenblockcoordinates = [[-1.1,1.02],[0.114,1.13],[-0.405,0.49],[-0.773,-1.09]]
    #once block detection code works, add loop to update map real-time

    # =mapping red block positions
    for i in range (len(redblockcoordinates)):
        displ.setColor(0XFF0000)
        blockx = redblockcoordinates[i][0]
        blocky = redblockcoordinates[i][1]

        pixelx = round((blockx-(-1.17))*scalex)
        pixely = round((-blocky+1.17)*scaley)
        displ.drawPixel(pixelx,pixely)
    
    #mapping green block positions
    for i in range (len(greenblockcoordinates)):
        displ.setColor(0X008000)
        blockx = greenblockcoordinates[i][0]
        blocky = greenblockcoordinates[i][1]

        pixelx = round((blockx-(-1.17))*scalex)
        pixely = round((-blocky+1.17)*scaley)
        displ.drawPixel(pixelx,pixely)


def plotting_stuff():
    #plotting after recording during main loop
    #dictionary of lists to iterate through
    things_to_plot = {'predicted distances':predicted_distance, 'long range sensor':real_longrange, 'short range sensor':real_shortrange}
    names_to_plot = list(things_to_plot.keys())
    arrays_to_plot = list(things_to_plot.values())

    #print(len(all_blocks_data))
    plt.clf()

    for i in range(len(things_to_plot)):
        name = names_to_plot[i]
        array = arrays_to_plot[i]
        tuple_list = list(zip(angles_list, array))
        tuple_list = sorted(tuple_list, key = lambda x:x[0])
        unzipped = (list(zip(*tuple_list)))

        plt.plot(unzipped[0], unzipped[1], label = name)

    #plotting where blocks are detected with alternating green and red scatters
    color = True
    for block in all_blocks_data:
        #print (block)
        block_angles = []
        if color == True:
            colour = 'green'
        else:
            colour = 'red'
        color = not color
        for data in block:
            block_angles.append(data[1])

        plt.scatter(block_angles, [0.75 for angle in block_angles], 10, c = colour)

    plt.ylim([0,3.5])
    plt.xlabel('angle pointing to/degrees')
    plt.ylabel('distance/m')
    plt.grid()
    plt.minorticks_on()
    plt.legend()
    plt.show()

def send_gps():    #from red to green: channel 2, red car always make way for green
    gps = gpsrecord()
    data = str(gps[0]) + ',' + str(gps[1])
    emitter.setChannel(2)
    data = data.encode('utf-8')
    emitter.send(data)

def list_to_str(x):
    y=""
    for coord in x:
        y += str(coord[0])
        y += ","
        y += str(coord[1])
        y += "!"
    return y

def str_to_list(y):
    coords=[]
    y=y.split('!')
    for item in y:
        if item != "":
            item = item.split(',')
            cod = [float(item[0]), float(item[1])]
            coords.append(cod)
    return coords
    
def send_blocks(data):
    data = list_to_str(data)
    emitter.setChannel(2)
    data = data.encode('utf-8')
    emitter.send(data)

def receive_blocks():
    robot.step(timestep)
    receiver.enable(timestep)
    receiver.setChannel(1)
    if receiver.getQueueLength() > 0:
        data = receiver.getData().decode('utf-8')
        data = str_to_list(data)
        receiver.nextPacket()
        return data
    else:
        return None
    

def receive():
    receiver.enable(timestep)
    receiver.setChannel(1)
    if receiver.getQueueLength() > 0:
        message = receiver.getData().decode('utf-8').split(',')
        message = [float(message[0]), float(message[1])]
        receiver.nextPacket()
        return message
    else:
        return None

def avoid_collision(thres = 0.5):
    red_coord = gpsrecord()
    green_coord = receive()
    #print(green_coord)
    if green_coord != None:
        distance = ((red_coord[0]-green_coord[0])**2 + (red_coord[1]-green_coord[1])**2)**0.5
        #print(distance)
        if distance < thres:
            print("oops")
            #Lmotor.setVelocity(0)    
            #Rmotor.setVelocity(0)     
            robot.step(timestep * 10)
    
    
# Main loop:
# - perform simulation steps until Webots is stopping the controller
red_stored = 0
green_stored = 0
red_obstacle_coords = []
green_obstacle_coords = []
while robot.step(timestep) != -1:
    #y IS THE NORTH-FACING DIRECTION WHEN THE GREEN SQUARE IS IN THE WEST AND THE RED IS IN THE EAST
    #a good camera angle for testing is 0.371, 0.852, 0.368, 4.54
    
    #updates all lists in the function:
    record_lists()
    
    #avoid_collision()
    
    
    origin = gpsrecord()
    all_blocks_data = scan360()
    red_remove_blocked(all_blocks_data)
  
    #GUInterface()
    for block in all_blocks_data:
        distances, angles = zip(*block)
        average_coords = givecoordofblock(distances, angles)
        red_obstacle_coords.append(average_coords)     
    #print("Red filtered: ", red_obstacle_coords, len(red_obstacle_coords))  
     
     
    #exchange coord info
    send_blocks(red_obstacle_coords)
    robot.step(timestep*1)
    other_blocks =  receive_blocks()
    #print("Red receive from green: ", other_blocks)
     
    all_obstacles = other_blocks + red_obstacle_coords
   
    
    all_obstacles = sort_by_angle(all_obstacles)
    
    #alternate order of picking from the other bot
    half = int(len(all_obstacles)/2)
    all_obstacles = all_obstacles[half:] + all_obstacles[:half]
    print(all_obstacles)
     
    for obstacle in all_obstacles:
        origin = gpsrecord()
        target = ring_coord(obstacle, radius=0.30) 
        moveto(target)
        if approach_and_catch(obstacle, colour ="RED", tol = 0.5, counters = 1):
            red_stored += 1
        print("red collected so far: ", red_stored)
        moveto(origin) 
        """        
        for obstacle in obstacle_coords:
            target = ring_coord(obstacle, radius=0.30) 
            moveto(target)
            if approach_and_catch(obstacle, colour ="RED", tol = 0.5, counters = 1):
                red_stored += 1
            print("red collected so far: ", red_stored)
            moveto(origin)     
        """     
 
    
    if red_stored == 4:
        break
    
  
    
    break


#plotting_stuff()